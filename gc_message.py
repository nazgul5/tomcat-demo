import re
import dateutil.parser
from humanfriendly import parse_size


def parse(message):
    """Guess GC message type and return dictionary with parsed tokens
    Args:
        message (string): GC log message line

    Returns:
        dict: parsed tokens
    """
    isotime_re = r'^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+([+-]\d{4}|Z))'

    # 2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  10198M->1220M(10200M), 5.4572036 secs]
    full_gc_re = re.compile(
        isotime_re +
        r':\s\d+\.\d+:\s\[Full\sGC\s\((.+)\)\s+' +
        r'(\d+)([BKMG])->(\d+)([BKMG])\((\d+)([BKMG])\),\s(\d+\.\d+)\ssecs\]'
    )

    tokens = full_gc_re.match(message)
    if tokens:
        return {
            'type': 'full-gc',
            'datetime': dateutil.parser.parse(tokens.group(1)),
            'cause': tokens.group(3),
            'heap_old': parse_size(tokens.group(4)+tokens.group(5)),
            'heap_new': parse_size(tokens.group(6)+tokens.group(7)),
            'heap_max': parse_size(tokens.group(8)+tokens.group(9)),
            'duration': float(tokens.group(10))

        }

    # 2017-03-01T17:08:13.478-0500: 6686.150: [GC pause (G1 Evacuation Pause) (mixed) (to-space exhausted), 1.1669987 secs]
    gc_pause_re = re.compile(
        isotime_re +
        r':\s\d+\.\d+:\s\[GC\spause\s' +
        r'\(([^\(\)]+)\)\s+\(([^\(\)]+)\)(\s+\(([^\(\)]+)\)|),' +
        r'\s(\d+\.\d+)\ssecs\]'
    )

    tokens = gc_pause_re.match(message)
    if tokens:
        return {
            'type': 'gc-pause',
            'datetime': dateutil.parser.parse(tokens.group(1)),
            'action': tokens.group(3),
            'collector': tokens.group(4),
            'cause': tokens.group(6),
            'duration': float(tokens.group(7))
        }

    # 2017-03-01T15:17:04.162-0500: 16.834: [GC concurrent-root-region-scan-end, 0.0250209 secs]
    other_re = re.compile(
        isotime_re +
        r':\s\d+\.\d+:\s\[GC\s' +
        r'(concurrent-root-region-scan-end|concurrent-mark-end|' +
        r'concurrent-cleanup-end|cleanup|remark)' +
        r'(.*),\s(\d+\.\d+)\ssecs\]'
    )

    tokens = other_re.match(message)
    if tokens:
        return {
            'type': 'other',
            'datetime': dateutil.parser.parse(tokens.group(1)),
            'action': tokens.group(3),
            'duration': float(tokens.group(5))
        }

    return None
