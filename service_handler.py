import boto3
import subprocess
import time
import random
from sys import stderr


def restart_service(linux_service_name, elb_name, healthy_hosts_threshold):
    """Functions checks whether ELB has number of healthy hosts greater than
    healthy_hosts_threshold and waits 60 seconds with random backoff for
    required number of hosts.

    Instances is not taken out of load-balancer and drained before service
    restart, so service disruption is expected.

    There is no distributed locking implement to prevent cluster to become
    in case of simultanious runs on all instances
    """

    elb = boto3.client('elb', region_name='us-east-2')

    tries = 10
    while tries > 0:
        instances = elb.describe_instance_health(LoadBalancerName=elb_name)
        healthy = [instance for instance in instances['InstanceStates']
                   if instance['State'] == 'InService']

        if len(healthy) < healthy_hosts_threshold:
            sleep = 60 + random.randint(0, 120)
            print("Not enough healthy hosts in the load balancer: %d < %d, sleeping %s seconds" %
                  (len(healthy), healthy_hosts_threshold, sleep),
                  file=stderr)
            sleep = 10 + random.randint(0, 120)
            time.sleep(sleep)
            tries -= 1
            continue
        else:
            # TODO: remove instance from Load-Balancer before restart
            # and add afterwards
            print("Invoking %s service restart" % linux_service_name,
                  file=stderr)
            subprocess.run(['service', linux_service_name, 'restart'])
            break
