```
usage: main.py [-h] [--logfile LOGFILE] [--tomcat_service TOMCAT_SERVICE]
               [--healthy_hosts HEALTHY_HOSTS] --load_balancer_name
               LOAD_BALANCER_NAME [--time_window TIME_WINDOW] [--debug DEBUG]

optional arguments:
  -h, --help            show this help message and exit
  --logfile LOGFILE     GC log file location
  --tomcat_service TOMCAT_SERVICE
                        Tomcat system service name
  --healthy_hosts HEALTHY_HOSTS
                        Number of healthy hosts in load-balancer
  --load_balancer_name LOAD_BALANCER_NAME
                        Load balancer name to check for healthy hosts
  --time_window TIME_WINDOW
                        Time windows to look for events, seconds
  --debug DEBUG         Enable debugging
```
