import unittest
import datetime
from dateutil.tz import tzoffset
from gc_message import parse


class TestGCMessageParser(unittest.TestCase):
    def test_full_gc(self):
        gc_message = '2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  10198M->1220M(10200M), 5.4572036 secs]'

        self.assertEqual(
            parse(gc_message),
            {
                'type': 'full-gc',
                'heap_max': 10200000000,
                'heap_new': 1220000000,
                'datetime': datetime.datetime(2017, 3, 1, 17, 8, 17, 86000,
                                              tzinfo=tzoffset(None, -18000)),
                'heap_old': 10198000000,
                'duration': 5.4572036,
                'cause': 'Allocation Failure'
            }
        )

    def test_gc_pause(self):
        gc_message = '2017-03-01T17:08:13.478-0500: 6686.150: [GC pause (G1 Evacuation Pause) (mixed) (to-space exhausted), 1.1669987 secs]'

        self.assertEqual(
            parse(gc_message),
            {
                'type': 'gc-pause',
                'action': 'G1 Evacuation Pause',
                'cause': 'to-space exhausted',
                'collector': 'mixed',
                'datetime': datetime.datetime(2017, 3, 1, 17, 8, 13, 478000,
                                              tzinfo=tzoffset(None, -18000)),
                'duration': 1.1669987
            }
        )

    def test_gc_pause_without_cause(self):
        gc_message = '2017-03-01T17:08:26.441-0500: 6699.114: [GC pause (G1 Evacuation Pause) (young), 0.0429834 secs]'

        self.assertEqual(
            parse(gc_message),
            {
                'action': 'G1 Evacuation Pause',
                'cause': None,
                'collector': 'young',
                'datetime': datetime.datetime(2017, 3, 1, 17, 8, 26, 441000,
                                              tzinfo=tzoffset(None, -18000)),
                'duration': 0.0429834,
                'type': 'gc-pause'
            }
        )

    def test_gc_other_remark(self):
        gc_message = '2017-03-01T21:42:43.856-0500: 23156.529: [GC remark 2017-03-01T21:42:43.856-0500: 23156.529: [Finalize Marking, 0.0009025 secs] 2017-03-01T21:42:43.857-0500: 23156.530: \
        [GC ref-proc, 0.0079152 secs] 2017-03-01T21:42:43.865-0500: 23156.537: [Unloading, 0.0527561 secs], 0.0710453 secs]'

        self.assertEqual(
            parse(gc_message),
            {
                'type': 'other',
                'action': 'remark',
                'datetime': datetime.datetime(2017, 3, 1, 21, 42, 43, 856000,
                                              tzinfo=tzoffset(None, -18000)),
                'duration': 0.0710453
            }
        )
