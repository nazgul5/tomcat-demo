import unittest
from freezegun import freeze_time
from gc_metrics_alarm import GCMetricsAlarm


class TestGCMetricsAlarm(unittest.TestCase):
    @freeze_time("2017-03-01T17:08:27")
    def test_message_ingest(self):
        gc_message = '2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  10198M->1220M(10200M), 5.4572036 secs]'

        gc_alarm = GCMetricsAlarm()
        (alert, gc_data) = gc_alarm.check(gc_message)
        self.assertEqual(alert, False)
        self.assertIsInstance(gc_data, dict)

    @freeze_time("2017-03-01T17:08:27")
    def test_gc_mem_freed_alert(self):
        gc_messages = [
            '2017-03-01T17:01:01.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:18.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:19.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:20.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:21.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:22.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:23.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:24.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:25.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:26.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:27.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
        ]

        gc_alarm = GCMetricsAlarm()
        for gc_message in gc_messages:
            (alert, gc_data) = gc_alarm.check(gc_message)

        self.assertEqual(alert, True)
        self.assertIsInstance(gc_data, dict)

        # Check old items has been removed
        self.assertEqual(len(gc_alarm.gc_events), 11)

    @freeze_time("2017-03-01T17:08:27")
    def test_gc_duration(self):
        gc_messages = [
            '2017-03-01T17:01:01.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 5.4572036 secs]',
            '2017-03-01T17:08:17.086-0500: 6689.759: [Full GC (Allocation Failure)  100M->90M(100M), 55.4572036 secs]',
        ]

        gc_alarm = GCMetricsAlarm()
        for gc_message in gc_messages:
            (alert, gc_data) = gc_alarm.check(gc_message)

        self.assertEqual(alert, True)
        self.assertIsInstance(gc_data, dict)
