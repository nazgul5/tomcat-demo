from sys import stderr
import datetime
from dateutil.tz import tzlocal
import gc_message


class GCMetricsAlarm(object):
    """Store events within time window and return positive
    alarm if GC memory clean or GC run time threshold are reached.

    Attributes:
        gc_events (list): sorted list of timeseries items with gc events
        window (int): time window to analyze events, in seconds
        gc_mem_freed_threshold (int): lowest percentage of mem freed for event
            to be accounted for threshold.
            Default: 20%
        gc_mem_count (int): number of events with gc_mem_freed_threshold during
            window time to trigger alert.
            Default: 10
        gc_max_total_duration (int): percentage of self.window time which is
            allowed for GC to be running, summarized by all events.
            Default: 15%
    """

    def __init__(self, window=300, gc_mem_freed_threshold=20, gc_mem_count=10,
                 gc_max_total_duration=15):
        self.gc_events = []
        self.window = window
        self.gc_mem_freed_threshold = gc_mem_freed_threshold
        self.gc_mem_count = gc_mem_count
        self.gc_max_total_duration = gc_max_total_duration

    def _append(self, dt, heap_old, heap_new, duration):
        """Appends GC event into the list and filter out outdated items
        Args:
            dt (datetime): GC event timestamp
            heap_old (int): pre-GC used heap size
            heap_new (int): after-GC used heap size
            gc_duration (float): GC operation duration

        Returns:
            tuple:
                (
                    bool: alert reached,
                    dict: parsed message
                )
        """
        self.gc_events.append({
            'datetime': dt,
            'heap_old': heap_old,
            'heap_new': heap_new,
            'duration': duration
        })
        current_time = datetime.datetime.now(tzlocal())
        self.gc_events = [ev for ev in self.gc_events if
                          int((dt-ev['datetime']).total_seconds()) <= self.window and
                          int((current_time-ev['datetime']).total_seconds()) <= self.window
                          ]

    def _check_gc_mem_threshold(self):
        weak_gc_runs = [ev for ev in self.gc_events if
                        (ev['heap_old']-ev['heap_new'])/ev['heap_old'] <
                        self.gc_mem_freed_threshold/100]

        if len(weak_gc_runs) > self.gc_mem_count:
            print("GC doesn't frees any mem", file=stderr)
            return True
        else:
            return False

    def _check_gc_total_duration(self):
        total_duration = 0
        for ev in self.gc_events:
            total_duration += ev['duration']

        if total_duration/self.window > self.gc_max_total_duration/100:
            print("GC duration time exceeded", file=stderr)
            return True
        else:
            return False

    def check(self, message):
        """Ingest GC message and determine if alert threshold is reached
        Args:
            message (string): GC log message line

        Returns:
            tuple:
                (
                    bool: alert reached,
                    dict: parsed message
                )
        """
        gc_data = gc_message.parse(message)

        if not gc_data or \
           int((datetime.datetime.now(tzlocal())-gc_data['datetime']).total_seconds()) > self.window:
            return (False, None)

        if gc_data.get('type') != 'full-gc':
            return (False, gc_data)

        # append new event to the list and delete outdated
        self._append(
            gc_data['datetime'],
            gc_data['heap_old'],
            gc_data['heap_new'],
            gc_data['duration']
        )

        return (
            self._check_gc_mem_threshold() or self._check_gc_total_duration(),
            gc_data
        )
