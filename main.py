#!/usr/bin/env python3
import re
import os
import pyinotify
import argparse
import json
from gc_metrics_alarm import GCMetricsAlarm
from service_handler import restart_service

DEBUG = False


def debug(message):
    if DEBUG:
        print(message)


def newrelic_log(gc_data):
    if not(gc_data and gc_data.get('type') and gc_data.get('datetime')):
        debug('newrelic_log: Invalid record: ' + str(gc_data))
        return
    else:
        debug('newrelic_log: ' + str(gc_data))

    newrelic_data = {
        'eventType': re.sub('[^A-z0-9_:]', '', gc_data['type']),
        'timestamp': int(gc_data['datetime'].strftime("%s")),
    }

    for key in (set(gc_data.keys()) - set(['type', 'datetime'])):
        newrelic_key = re.sub('[^A-z0-9_:]', '', key)
        newrelic_data[newrelic_key] = gc_data[key]

    with open('/tmp/newrelic_insights.log', 'a') as f:
        f.write(json.dumps(newrelic_data, sort_keys=True, indent=2) + '\n')


class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, *args, **kwargs):
        super(EventHandler, self).__init__(*args, **kwargs)
        self.cmd_args = kwargs['cmd_args']
        self.filename = kwargs['cmd_args'].logfile
        self.file = open(self.filename, 'r')
        self.position = 0
        self.gc_alarm = GCMetricsAlarm(kwargs['cmd_args'].time_window)
        self.read_lines()

    def process_IN_MODIFY(self, event):
        debug('IN_MODIFY: ' + os.path.join(event.path, event.name))
        if self.filename not in os.path.join(event.path, event.name):
            return
        else:
            self.read_lines()

    def process_IN_CREATE(self, event):
        debug('IN_CREATE: ' + os.path.join(event.path, event.name))
        if self.filename in os.path.join(event.path, event.name):
            # file has been rotated
            self.file.close
            self.file = open(self.filename, 'r')
            self.position = 0
            self.read_lines()

    def read_lines(self):
        new_lines = self.file.read()
        last_n = new_lines.rfind('\n')
        if last_n >= 0:
            self.position += last_n + 1
            self.process_lines(new_lines.rstrip().split('\n'))
        self.file.seek(self.position)

    def process_lines(self, lines):
        for line in lines:
            (alarm, gc_data) = self.gc_alarm.check(line)
            newrelic_log(gc_data)

        if alarm:
            restart_service(self.cmd_args.tomcat_service,
                            self.cmd_args.load_balancer_name,
                            self.cmd_args.healthy_hosts
                            )

        # Empty buffer
        self.gc_alarm = GCMetricsAlarm(self.cmd_args.time_window)

        # Rewind file reader position
        self.position = 0


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--logfile', type=str, required=False,
                        default='/var/log/tomcat8/gc.log.0.current',
                        help='GC log file location')
    parser.add_argument('--tomcat_service', type=str, required=False,
                        default='tomcat8',
                        help='Tomcat system service name')
    parser.add_argument('--healthy_hosts', type=int, required=False,
                        default=3,
                        help='Number of healthy hosts in load-balancer')
    parser.add_argument('--load_balancer_name', type=str, required=True,
                        default=None,
                        help='Load balancer name to check for healthy hosts')
    parser.add_argument('--time_window', type=int, required=False,
                        default=300,
                        help='Time windows to look for events, seconds')
    parser.add_argument('--debug', type=bool, required=False,
                        default=False,
                        help='Enable debugging')
    args = parser.parse_args()

    global DEBUG
    DEBUG = args.debug

    wm = pyinotify.WatchManager()
    handler = EventHandler(cmd_args=args)
    notifier = pyinotify.Notifier(wm, handler)

    # Check if new data were written to the file
    wm.add_watch(args.logfile, pyinotify.IN_MODIFY)
    # Check directory for the modifications to detect log file rotation
    wm.add_watch(os.path.dirname(args.logfile),
                 pyinotify.IN_MODIFY | pyinotify.IN_CREATE)

    notifier.loop()


if __name__ == '__main__':
    main()
